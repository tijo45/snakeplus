import UIKit
import MultipeerConnectivity

@available(iOS 8.0, *)
class ViewController: UIViewController, SnakeViewDelegate, MCSessionDelegate, MCBrowserViewControllerDelegate {
	
    @IBOutlet var startButton:UIButton?
    @IBOutlet var sessionButton:UIButton?
    
	var snakeView:SnakeView?
	var timer:NSTimer?
    //https://www.hackingwithswift.com/example-code/system/how-to-create-a-peer-to-peer-network-using-the-multipeer-connectivity-framework
    var peerID: MCPeerID!
    var mcSession: MCSession!
    var mcAdvertiserAssistant: MCAdvertiserAssistant!

    var hostFlag: Bool?
	var snake:Snake?
	var fruit:Point?
    

	override func viewDidLoad() {
		super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: #selector(ViewController.showConnectionPrompt))
        
        self.peerID = MCPeerID(displayName: UIDevice.currentDevice().name)
        self.mcSession = MCSession(peer: peerID, securityIdentity: nil, encryptionPreference: .Required)
        self.mcSession.delegate = self
        
		self.snakeView = SnakeView(frame: self.view.bounds)
		self.snakeView!.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
		self.view.insertSubview(self.snakeView!, atIndex: 0)

		if let view = self.snakeView {
			view.delegate = self
		}
		for direction in [UISwipeGestureRecognizerDirection.Right,
			UISwipeGestureRecognizerDirection.Left,
			UISwipeGestureRecognizerDirection.Up,
			UISwipeGestureRecognizerDirection.Down] {
				let gr = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.swipe(_:)))
				gr.direction = direction
				self.view.addGestureRecognizer(gr)
		}
	}
    
    func showConnectionPrompt() {
        let ac = UIAlertController(title: "Connect to others", message: nil, preferredStyle: .ActionSheet)
        ac.addAction(UIAlertAction(title: "Host a session", style: .Default, handler: startHosting))
        ac.addAction(UIAlertAction(title: "Join a session", style: .Default, handler: joinSession))
        ac.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        presentViewController(ac, animated: true, completion: nil)
    }


	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
    
    func session(session: MCSession, didReceiveStream stream: NSInputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, withProgress progress: NSProgress) {
        
    }
    
    func session(session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, atURL localURL: NSURL, withError error: NSError?) {
        
    }
    
    func browserViewControllerDidFinish(browserViewController: MCBrowserViewController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func browserViewControllerWasCancelled(browserViewController: MCBrowserViewController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
/*
    func sendImage(img: UIImage) {
        if mcSession.connectedPeers.count > 0 {
            if let imageData = UIImagePNGRepresentation(img) {
                do {
                    try mcSession.sendData(imageData, toPeers: mcSession.connectedPeers, withMode: .Reliable)
                } catch let error as NSError {
                    let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .Alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                    presentViewController(ac, animated: true, completion: nil)
                }
            }
        }
    }

 */   
    
    func session(session: MCSession, didReceiveData data: NSData, fromPeer peerID: MCPeerID) {
    /*
        switch data {
        case Direction.right:
            if (self.snake2?.changeDirection(Direction.right) != nil) {
                self.snake2?.lockDirection()
            }
        case Direction.left:
            if (self.snake2?.changeDirection(Direction.left) != nil) {
                self.snake2?.lockDirection()
            }
        case Direction.up:
            if (self.snake2?.changeDirection(Direction.up) != nil) {
                self.snake2?.lockDirection()
            }
        case Direction.down:
            if (self.snake2?.changeDirection(Direction.down) != nil) {
                self.snake2?.lockDirection()
            }
        default:
            assert(false, "This could not happen")
        }
      */
        
    /*    if let image = UIImage(data: data) {
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                // do something with the image
            }
        }
    */
    }

    func startHosting(action: UIAlertAction!) {
        hostFlag = true;
        mcAdvertiserAssistant = MCAdvertiserAssistant(serviceType: "hws-kb", discoveryInfo: nil, session: mcSession)
        mcAdvertiserAssistant.start()
    }
    
    func joinSession(action: UIAlertAction!) {
        hostFlag = false;
        let mcBrowser = MCBrowserViewController(serviceType: "hws-kb", session: mcSession)
        mcBrowser.delegate = self
        presentViewController(mcBrowser, animated: true, completion: nil)
    }


    
    func session(session: MCSession, peer peerID: MCPeerID, didChangeState state: MCSessionState) {
        switch state {
        case MCSessionState.Connected:
            print("Connected: \(peerID.displayName)")
            
        case MCSessionState.Connecting:
            print("Connecting: \(peerID.displayName)")
            
        case MCSessionState.NotConnected:
            print("Not Connected: \(peerID.displayName)")
        }
    }

    


	func swipe (gr:UISwipeGestureRecognizer) {
		let direction = gr.direction
		switch direction {
		case UISwipeGestureRecognizerDirection.Right:
			if (self.snake?.changeDirection(Direction.right) != nil) {
				self.snake?.lockDirection()
			}
		case UISwipeGestureRecognizerDirection.Left:
			if (self.snake?.changeDirection(Direction.left) != nil) {
				self.snake?.lockDirection()
			}
		case UISwipeGestureRecognizerDirection.Up:
			if (self.snake?.changeDirection(Direction.up) != nil) {
				self.snake?.lockDirection()
			}
		case UISwipeGestureRecognizerDirection.Down:
			if (self.snake?.changeDirection(Direction.down) != nil) {
				self.snake?.lockDirection()
			}
		default:
			assert(false, "This could not happen")
		}
	}

	func makeNewFruit() {
		srandomdev()
		let worldSize = self.snake!.worldSize
		var x = 0, y = 0
		while (true) {
			x = random() % worldSize.width
			y = random() % worldSize.height
			var isBody = false
			for p in self.snake!.points {
				if p.x == x && p.y == y {
					isBody = true
					break
				}
			}
			if !isBody {
				break
			}
		}
		self.fruit = Point(x: x, y: y)
	}

	func startGame() {
		if (self.timer != nil) {
			return
		}
        
		self.startButton!.hidden = true
        self.sessionButton!.hidden = true
		let worldSize = WorldSize(width: 24, height: 15)
		self.snake = Snake(inSize: worldSize, length: 2)
		self.makeNewFruit()
		self.timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(ViewController.timerMethod(_:)), userInfo: nil, repeats: true)
		self.snakeView!.setNeedsDisplay()
	}

	func endGame() {
		self.startButton!.hidden = false
        self.sessionButton!.hidden = false
		self.timer!.invalidate()
		self.timer = nil
	}

	func timerMethod(timer:NSTimer) {
		self.snake?.move()
		let headHitBody = self.snake?.isHeadHitBody()
		if headHitBody == true {
			self.endGame()
			return
		}

		let head = self.snake?.points[0]
		if head?.x == self.fruit?.x &&
			head?.y == self.fruit?.y {
				self.snake!.increaseLength(2)
				self.makeNewFruit()
		}

		self.snake?.unlockDirection()
		self.snakeView!.setNeedsDisplay()
	}

	@IBAction func start(sender:AnyObject) {
		self.startGame()
	}
    
    @IBAction func startSessiondiaglog(sender:AnyObject) {
        self.showConnectionPrompt()
    }

	func snakeForSnakeView(view:SnakeView) -> Snake? {
		return self.snake
	}
	func pointOfFruitForSnakeView(view:SnakeView) -> Point? {
		return self.fruit
	}
}
